sap.ui.define([
	"sap/ui/core/Control"
], function(Control) {
	"use strict";

	return Control.extend("com.task.control.Tile", {

		metadata: {
			properties: {
				width: {
					type: "sap.ui.core.CSSSize" //this is optional, but it helps prevent errors in your code by enforcing a type
						//this is also optional, but recommended, as it prevents your properties being null
				},
				height: {
					type: "sap.ui.core.CSSSize"
				}
			},
			aggregations: {
				content: {
					type: "sap.ui.core.Control"
				}
			},
			defaultAggregation: "content"
		},

		init: function() {},

		renderer: function(oRm, oControl) {
			oRm.write("<div");
			oRm.write(" style=\"width: " + oControl.getWidth() + "; height: " + oControl.getHeight() + ";\"");
			oRm.writeControlData(oControl);
			oRm.write(">");
			$(oControl.getContent()).each(function() {
				oRm.renderControl(this);
			});
			oRm.write("</div>");
		}
	});

});