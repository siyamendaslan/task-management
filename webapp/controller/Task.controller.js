var oComment = [];
sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel"
], function(Controller, JSONModel) {
	"use strict";

	return Controller.extend("com.task.controller.Task", {

		onInit: function() {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.getRoute("Task").attachPatternMatched(this._onObjectMatched, this);
		},
		onAfterRendering: function() {
			if (sap.ui.Device.system.phone) {
				this.getView().byId("detail").setShowNavButton(true);
				var oSplitCont = this.getView().byId("master"),
					ref = oSplitCont.getDomRef() && oSplitCont.getDomRef().parentNode;
				// set all parent elements to 100% height, this should be done by app developer, but just in case
				if (ref && !ref._sapUI5HeightFixed) {
					ref._sapUI5HeightFixed = true;
					while (ref && ref !== document.documentElement) {
						var $ref = jQuery(ref);
						if ($ref.attr("data-sap-ui-root-content")) { // Shell as parent does this already
							break;
						}
						if (!ref.style.height) {
							ref.style.height = "100%";
						}
						ref = ref.parentNode;
					}
				}

			}

		},

		onSelectionChange: function(oEvent) {
			//Find Selected project
			var projectId = oEvent.getParameter("listItem").getBindingContext("taskData").getProperty("projectId");
			// filter tile with projectId
			var tile = this.getView().byId("TileContainerExpanded").getBinding("content");
			var tile1 = this.getView().byId("TileContainerExpanded2").getBinding("content");
			var tile2 = this.getView().byId("TileContainerExpanded3").getBinding("content");
			var filter = new sap.ui.model.Filter("projectId", sap.ui.model.FilterOperator.EQ, projectId);
			tile.filter([filter]);
			tile1.filter([filter]);
			tile2.filter([filter]);
			if (sap.ui.Device.system.phone) {
				this.getView().byId("Split").toDetail(this.createId("detail"));
			}

		},
		onNewProject: function(oEvent) {
			this._getNewEditProjectDialog().open();
			sap.ui.core.Fragment.byId("frgNewEditProject", "NewEditProjectDialog").setTitle(this.getView().getModel("i18n").getResourceBundle()
				.getText(
					"NewProject"));
		},
		onEditProject: function(oEvent) {

			this._getNewEditProjectDialog().open();
			sap.ui.core.Fragment.byId("frgNewEditProject", "NewEditProjectDialog").setTitle(this.getView().getModel("i18n").getResourceBundle()
				.getText(
					"EditProject"));
			if (this.getView().byId("projectList").getSelectedItem() === null) {
				// this for after first render page and user not selected any item.
				sap.ui.core.Fragment.byId("frgNewEditProject", "projectId").setValue(this.getView().byId("projectList").getAggregation("items")[0]
					.getBindingContext("taskData").getProperty("projectId"));
				sap.ui.core.Fragment.byId("frgNewEditProject", "title").setValue(this.getView().byId("projectList").getAggregation("items")[0]
					.getBindingContext("taskData").getProperty("title"));
			} else {
				sap.ui.core.Fragment.byId("frgNewEditProject", "projectId").setValue(this.getView().byId("projectList").getSelectedItem().getBindingContext(
					"taskData").getProperty("projectId"));
				sap.ui.core.Fragment.byId("frgNewEditProject", "title").setValue(this.getView().byId("projectList").getSelectedItem().getBindingContext(
					"taskData").getProperty("title"));
			}
		},
		onDeleteProject: function(oEvent) {
			var selectedId;
			if (this.getView().byId("projectList").getSelectedItem() === null) {
				selectedId = this.getView().byId("projectList").getAggregation("items")[0]
					.getBindingContext("taskData").getProperty("projectId");
			} else {
				selectedId = this.getView().byId("projectList").getSelectedItem().
				getBindingContext("taskData").getProperty("projectId");
			}
			//Mapping Project
			var deletedIndex = this.getView().getModel("taskData").getData().Project.map(function(e) {
				return e.projectId;
			}).indexOf(selectedId);
			this.getView().getModel("taskData").getData().Project.splice(deletedIndex, 1);
			//Also we have delete dependcy Task 
			var filteredTask = this.getView().getModel("taskData").getData().Task.filter(function(el) {
				return el.projectId !== selectedId;
			});
			// Delete related task
			this.getView().getModel("taskData").getData().Task = filteredTask;
			//	this.getView().getModel("taskData").getData().Task.splice(deletedIndex, 1);
			this.getView().getModel("taskData").refresh(true);
			var projectId = this.getView().byId("projectList").getAggregation("items")[0]
				.getBindingContext("taskData").getProperty("projectId");
			var tile = this.getView().byId("TileContainerExpanded").getBinding("content");
			var tile1 = this.getView().byId("TileContainerExpanded2").getBinding("content");
			var tile2 = this.getView().byId("TileContainerExpanded3").getBinding("content");
			var filter = new sap.ui.model.Filter("projectId", sap.ui.model.FilterOperator.EQ, projectId);
			tile.filter([filter]);
			tile1.filter([filter]);
			tile2.filter([filter]);
		},
		onProjectDialogSave: function(oEvent) {
			if (sap.ui.core.Fragment.byId("frgNewEditProject", "title").getValue() === null || sap.ui.core.Fragment.byId("frgNewEditProject",
					"title").getValue() === "") {
				sap.m.MessageToast.show(this.getView().getModel("i18n").getResourceBundle().getText(
					"ProjectTitleError"));
			} else {

				if (sap.ui.core.Fragment.byId("frgNewEditProject", "projectId").getValue() === "" || sap.ui.core.Fragment.byId("frgNewEditProject",
						"projectId").getValue() === null) {
					// new Request
					var projectId = this.getView().getModel("taskData").getData().Project.length;

					this.getView().getModel("taskData").getData().Project.push({
						projectId: parseInt(this.getView().getModel("taskData").getData().Project[projectId - 1].projectId) + 1,
						title: sap.ui.core.Fragment.byId("frgNewEditProject", "title").getValue(),

					});

				} else {
					// Edit Request
					var Id = sap.ui.core.Fragment.byId("frgNewEditProject", "projectId").getValue();
					var editTask = this.getView().getModel("taskData").getData().Project.findIndex(function(el) {
						return el.projectId === Id;
					});
					this.getView().getModel("taskData").getData().Project[editTask].title = sap.ui.core.Fragment.byId("frgNewEditProject", "title").getValue();
				}
				this._oNewEditProjectDialog.close();
				this.getView().getModel("taskData").refresh(true);
			}
		},
		onProjectDialogCancel: function(oEvent) {
			this._oNewEditProjectDialog.close();
		},
		onEditTask: function(oEvent) {
			oComment = [];
			this._getNewEditDialog().open();
			sap.ui.core.Fragment.byId("frgNewEdit", "NewEditDialog").setTitle(this.getView().getModel("i18n").getResourceBundle().getText(
				"EditTask"));
			this._oNewEditDialog.setModel(this.getView().getModel("taskData"));
			sap.ui.core.Fragment.byId("frgNewEdit", "title").setValue(oEvent.getSource().getBindingContext("taskData").getProperty(
				"title"));
			sap.ui.core.Fragment.byId("frgNewEdit", "taskId").setValue(oEvent.getSource().getBindingContext("taskData").getProperty(
				"taskId"));
			sap.ui.core.Fragment.byId("frgNewEdit", "description").setValue(oEvent.getSource().getBindingContext("taskData").getProperty(
				"description"));

			// Selected key begin
			sap.ui.core.Fragment.byId("frgNewEdit", "state").setSelectedKey(oEvent.getSource().getBindingContext("taskData").getProperty(
				"stateId"));
			sap.ui.core.Fragment.byId("frgNewEdit", "priority").setSelectedKey(oEvent.getSource().getBindingContext("taskData").getProperty(
				"priorityId"));
			sap.ui.core.Fragment.byId("frgNewEdit", "type").setSelectedKey(oEvent.getSource().getBindingContext("taskData").getProperty(
				"typeId"));
			sap.ui.core.Fragment.byId("frgNewEdit", "project").setSelectedKey(oEvent.getSource().getBindingContext("taskData").getProperty(
				"projectId"));
			// Selected key end
			var commentLength = oEvent.getSource().getBindingContext("taskData").getProperty("comment").length;
			//Task comment control for edit process.
			if (commentLength > 0) {
				for (var i = 0; i < commentLength; i++) {
					oComment.push({
						commentId: oEvent.getSource().getBindingContext("taskData").getProperty("comment")[i].commentId,
						title: oEvent.getSource().getBindingContext("taskData").getProperty("comment")[i].title,
					});
					this.onAddComment(oEvent, oEvent.getSource().getBindingContext("taskData").getProperty("comment")[i].title);
				}
			}

		},
		onDelete: function(oEvent) {
			var deletedTask = oEvent.getSource().getBindingContext("taskData").getProperty("taskId");
			var filteredTask = this.getView().getModel("taskData").getData().Task.filter(function(el) {
				return el.taskId !== deletedTask;
			});
			this.getView().getModel("taskData").getData().Task = filteredTask;
			this.getView().getModel("taskData").refresh(true);
		},
		onNewTask: function(oEvent) {
			// new Task Comment
			oComment = [];
			this._getNewEditDialog().open();
			sap.ui.core.Fragment.byId("frgNewEdit", "NewEditProjectDialog").setTitle(this.getView().getModel("i18n").getResourceBundle().getText(
				"NewTask"));
		},

		onProjectSearch: function(oEvent) {
			this.getView().byId("projectList").setBusy(true);
			var oList = this.getView().byId("projectList").getBinding("items");
			var sString = oEvent.getSource().getProperty("value");
			var filter = new sap.ui.model.Filter("title", sap.ui.model.FilterOperator.Contains, sString);
			oList.filter([filter]);
			this.getView().byId("projectList").setBusy(false);
		},
		onAddComment: function(oEvent, comment) {
			// if comment is undefined request come from new comment otherwise request come from json model
			var Id;
			var that = this;
			if (comment === undefined) {

				comment = sap.ui.core.Fragment.byId("frgNewEdit", "comment").getValue();
				if (comment === null || comment === "") {
					sap.m.MessageToast.show(this.getView().getModel("i18n").getResourceBundle().getText(
						"CommentError"));
				} else {
					// add comment JSON model if the comment new
					// comment id always get next number 
					if (sap.ui.core.Fragment.byId("frgNewEdit", "taskId").getValue() === "" || sap.ui.core.Fragment.byId("frgNewEdit", "taskId").getValue() ===
						null) {
						// new Task
						oComment.push({
							"commentId": oComment.length.toString(),
							"title": comment
						});
					} else {
						//Edit Task
						Id = sap.ui.core.Fragment.byId("frgNewEdit", "taskId").getValue();
						Id = this.getView().getModel("taskData").getData().Task[Id].comment.length.toString();
						oComment.push({
							"commentId": Id,
							"title": comment
						});
					}
					sap.ui.core.Fragment.byId("frgNewEdit", "NewEdit").addFormElement(new sap.ui.layout.form.FormElement({
						label: "Comment",
						fields: [
							new sap.m.Text({
								text: comment
							}),
							new sap.m.Button({
								icon: "sap-icon://delete",
								type: "Reject",
								width: "20%",
								press: that.onCommentDelete
							})
						]
					}));
					sap.ui.core.Fragment.byId("frgNewEdit", "comment").setValue("");
				}
			} else {

				// Form element add in dialog for each comment
				sap.ui.core.Fragment.byId("frgNewEdit", "NewEdit").addFormElement(new sap.ui.layout.form.FormElement({
					label: "Comment",
					fields: [
						new sap.m.Text({
							text: comment
						}),
						new sap.m.Button({
							icon: "sap-icon://delete",
							type: "Reject",
							width: "20%",
							press: that.onCommentDelete
						})
					]
				}));
			}
		},
		onCommentDelete: function(oEvent) {

			var title = oEvent.getSource().getParent().getAggregation("fields")[0].getText();
			var filteredTask = oComment.filter(function(el) {
				return el.title !== title;
			});
			oComment = filteredTask;

			// delete in oComment
			oEvent.getSource().getParent().destroy();
		},
		onDialogCancel: function() {
			this._oNewEditDialog.close();
		},
		onDialogSave: function(oEvent) {
			// Title check
			if (sap.ui.core.Fragment.byId("frgNewEdit", "title").getValue() === null || sap.ui.core.Fragment.byId("frgNewEdit", "title").getValue() ===
				"") {
				sap.m.MessageToast.show(this.getView().getModel("i18n").getResourceBundle().getText(
					"TaskTitleError"));
			} else {
				if (sap.ui.core.Fragment.byId("frgNewEdit", "taskId").getValue() === "" || sap.ui.core.Fragment.byId("frgNewEdit", "taskId").getValue() ===
					null) {
					// new Request
					var taskId = this.getView().getModel("taskData").getData().Task.length;

					this.getView().getModel("taskData").getData().Task.push({
						taskId: parseInt(this.getView().getModel("taskData").getData().Task[taskId - 1].taskId) + 1,
						title: sap.ui.core.Fragment.byId("frgNewEdit", "title").getValue(),
						projectId: sap.ui.core.Fragment.byId("frgNewEdit", "project").getSelectedKey(),
						description: sap.ui.core.Fragment.byId("frgNewEdit", "description").getValue(),
						comment: oComment,
						stateId: sap.ui.core.Fragment.byId("frgNewEdit", "state").getSelectedKey(),
						priorityId: sap.ui.core.Fragment.byId("frgNewEdit", "priority").getSelectedKey(),
						type: sap.ui.core.Fragment.byId("frgNewEdit", "type").getSelectedKey()

					});

				} else {
					// Edit Request
					var Id = sap.ui.core.Fragment.byId("frgNewEdit", "taskId").getValue();
					var editTask = this.getView().getModel("taskData").getData().Task.findIndex(function(el) {
						return el.taskId === Id;
					});
					this.getView().getModel("taskData").getData().Task[editTask].projectId = sap.ui.core.Fragment.byId("frgNewEdit", "project").getSelectedKey();
					this.getView().getModel("taskData").getData().Task[editTask].typeId = sap.ui.core.Fragment.byId("frgNewEdit", "type").getSelectedKey();
					this.getView().getModel("taskData").getData().Task[editTask].stateId = sap.ui.core.Fragment.byId("frgNewEdit", "state").getSelectedKey();
					this.getView().getModel("taskData").getData().Task[editTask].priorityId = sap.ui.core.Fragment.byId("frgNewEdit", "priority").getSelectedKey();
					this.getView().getModel("taskData").getData().Task[editTask].comment = oComment;
					this.getView().getModel("taskData").getData().Task[editTask].title = sap.ui.core.Fragment.byId("frgNewEdit", "title").getValue();
					this.getView().getModel("taskData").getData().Task[editTask].description = sap.ui.core.Fragment.byId("frgNewEdit",
							"description")
						.getValue();

				}
				this._oNewEditDialog.close();
				this.getView().getModel("taskData").refresh(true);
			}
		},
		// this method for sap fiori client. in mobile master and detail are different views we have to do this.
		onPressDetailBack: function(oEvent) {
			this.getView().byId("Split").toMaster(this.createId("master"));
		},

		_getNewEditDialog: function() {

			if (!this._oNewEditDialog) {
				this._oNewEditDialog = sap.ui.xmlfragment("frgNewEdit", "com.task.fragment.NewEdit", this);
				this.getView().addDependent(this._oNewEditDialog);
				jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oNewEditDialog);
			} else {
				// if the edit dialog open before we have to destroy it then call again.
				//Because I created Dynamic comment area.
				this._oNewEditDialog.destroy();
				this._oNewEditDialog = sap.ui.xmlfragment("frgNewEdit", "com.task.fragment.NewEdit", this);
				this.getView().addDependent(this._oNewEditDialog);
				jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oNewEditDialog);

			}

			return this._oNewEditDialog;

		},
		_getNewEditProjectDialog: function() {

			if (!this._oNewEditProjectDialog) {
				this._oNewEditProjectDialog = sap.ui.xmlfragment("frgNewEditProject", "com.task.fragment.NewEditProject", this);
				this.getView().addDependent(this._oNewEditProjectDialog);
				jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oNewEditProjectDialog);
			} else {
				// if the edit dialog open before we have to destroy it then call again.
				//Because I created Dynamic comment area.
				this._oNewEditProjectDialog.destroy();
				this._oNewEditProjectDialog = sap.ui.xmlfragment("frgNewEditProject", "com.task.fragment.NewEditProject", this);
				this.getView().addDependent(this._oNewEditProjectDialog);
				jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oNewEditProjectDialog);

			}
			this._oNewEditProjectDialog.setModel(this.getView().getModel("taskData"));
			return this._oNewEditProjectDialog;

		},

		_onObjectMatched: function(oEvent) {
			var userName = oEvent.getParameter("arguments").userName;
			this.getView().byId("master").setTitle(userName + '\'s Projects');
			// get the first item to list for filter
			var firstItem = this.getView().byId("projectList").getAggregation("items")[0]
				.getBindingContext("taskData").getProperty("projectId");
			var tile = this.getView().byId("TileContainerExpanded").getBinding("content");
			var filter = new sap.ui.model.Filter("projectId", sap.ui.model.FilterOperator.EQ, firstItem);
			tile.filter([filter]);
			oComment = [];
		}

	});
});