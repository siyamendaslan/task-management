sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel"
], function(Controller, JSONModel) {
	"use strict";

	return Controller.extend("com.task.controller.Login", {

		onInit: function() {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.getRoute("Login").attachPatternMatched(this._onObjectMatched, this);
		},
		onLogin: function(oEvent) {
			let userName = this.getView().byId("userName").getValue();

			if (userName === null || userName === "") {
				sap.m.MessageToast.show(this.getView().getModel("i18n").getResourceBundle().getText(
					"EmptyUser"));
			} else {
				this.getRouter().navTo("Task", {
					userName: userName
				});
			}

		},
		getRouter: function() {
			return sap.ui.core.UIComponent.getRouterFor(this);
		},

	});
});